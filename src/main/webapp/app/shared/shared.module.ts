import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TruequearSharedLibsModule, TruequearSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [TruequearSharedLibsModule, TruequearSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [TruequearSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TruequearSharedModule {
  static forRoot() {
    return {
      ngModule: TruequearSharedModule
    };
  }
}
